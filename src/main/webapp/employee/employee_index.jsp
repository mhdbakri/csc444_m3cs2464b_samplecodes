<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>List of employees</title>
</head>
<body>
	<h1>Employee List</h1>
	<p>
		<a href="EmployeeServlet?action=create">Insert new Employee</a>
	</p>
	<table border=1>
		<thead>
			<tr>
				<td>ID</td>
				<td>Name</td>
				<td>Hire Date</td>
				<td>Manager</td>
				<td>Action</td>
				<td>Dependents</td>
			</tr>
		</thead>
		<tbody>
			<%-- loop and display employee detail in each row --%>
			<c:forEach var="emp" items="${employees}">
			<tr>
				<td>${emp.employee_id}</td>
				<td><a href="EmployeeServlet?action=view&id=${emp.employee_id}">${emp.first_name} ${emp.last_name}</a></td>
				<td><fmt:formatDate value="${emp.hire_date}" pattern="dd-MM-yyyy" /></td>
				<td>${emp.manager.first_name } ${emp.manager.last_name }</td>
				<td>
					<a href="EmployeeServlet?action=delete&id=${emp.employee_id}">Delete</a>&nbsp;
					<a href="EmployeeServlet?action=edit&id=${emp.employee_id}">Update</a>
				</td>
				<td>
					<a href="">View Dependents</a>
				</td>
				
			</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>