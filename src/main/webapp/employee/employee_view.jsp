<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>${empl.first_name } ${empl.last_name }</title>
</head>
<%-- Display single Employee info --%>
<body>
	<table>
		<tr>
			<td>Employee ID:</td>
			<td><b>${empl.employee_id }</b></td>
		</tr>
		<tr>
			<td>First Name:</td>
			<td><b>${empl.first_name }</b></td>
		</tr>
		<tr>
			<td>Last Name:</td>
			<td><b>${empl.last_name }</b></td>
		</tr>
		<tr>
			<td>E-mail:</td>
			<td><b>${empl.email }</b></td>
		</tr>
		<tr>
			<td>Phone Number:</td>
			<td><b>${empl.phone_number }</b></td>
		</tr>
		<tr>
			<td>Hire Date:</td>
			<td><b><fmt:formatDate value="${empl.hire_date}" pattern="yyyy-MM-dd" /></b></td>
		</tr>
		<tr>
			<td>Salary:</td>
			<td><b>${empl.salary }</b></td>
		</tr>
		<tr>
			<td>Job:</td>
			<td><b>${empl.job_id }</b></td>
		</tr>
		<tr>
			<td>Manager:</td>
			<td><b>${empl.manager.first_name } ${empl.manager.last_name }</b></td>
		</tr>
		<tr>
			<td>Department:</td>
			<td><b>${empl.department_id }</b></td>
		</tr>
	</table>
	<a href="EmployeeServlet?action=list">Back</a>
</body>
</html>