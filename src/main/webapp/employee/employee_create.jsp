<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert new Employee record</title>
</head>
<%-- Display insert Employee form --%>
<body>
	<h1>Insert new Employee record</h1>
	<form action="EmployeeServlet?action=store" method="POST">
	<table>
		<tr>
			<td>First Name:</td>
			<td><input type="text" name="firstName"/></td>
		</tr>
		<tr>
			<td>Last Name:</td>
			<td><input type="text" name="lastName" required/></td>
		</tr>
		<tr>
			<td>E-mail:</td>
			<td><input type="email" name="email" required/></td>
		</tr>
		<tr>
			<td>Phone Number:</td>
			<td><input type="text" name="phoneNumber"/></td>
		</tr>
		<tr>
			<td>Hire Date:</td>
			<td><input type="date" name="hireDate" /></td>
		</tr>
		<tr>
			<td>Salary:</td>
			<td><input type="number" name="salary" required/></td>
		</tr>
		<tr>
			<td>Job:</td>
			<td>
				<select name="jobId" required>
					<option value="">Select Job</option>
					<c:forEach var="job" items="${jobs}">
						<option value="${job.jobId}">${job.jobTitle}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>Manager:</td>
			<td>
				<select name="managerId">
					<option value="">Select Manager</option>
					<c:forEach var="manager" items="${managers}">
						<option value="${manager.employee_id}">${manager.first_name} ${manager.last_name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>Department:</td>
			<td>
				<select name="departmentId">
					<option value="">Select Department</option>
					<c:forEach var="department" items="${departments}">
						<option value="${department.departmentId}">${department.departmentName}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td align="right"><input type="submit" value="SUBMIT"/></td>
		</tr>
	</table>
	</form>
	<a href="EmployeeServlet?action=list">Back</a>
</body>
</html>