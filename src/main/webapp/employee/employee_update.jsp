<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert new Employee record</title>
</head>
<%-- Display edit Employee form --%>
<body>
	<h1>Update Employee record [ID: ${emp.employee_id }]</h1>
	<form action="EmployeeServlet?action=update" method="POST">
	<table>
		<tr>
			<td>First Name:</td>
			<td><input type="text" name="firstName" value="${emp.first_name }"/></td>
		</tr>
		<tr>
			<td>Last Name:</td>
			<td><input type="text" name="lastName" value="${emp.last_name }" required/></td>
		</tr>
		<tr>
			<td>E-mail:</td>
			<td><input type="email" name="email" value="${emp.email }" required/></td>
		</tr>
		<tr>
			<td>Phone Number:</td>
			<td><input type="text" name="phoneNumber" value="${emp.phone_number }"/></td>
		</tr>
		<tr>
			<td>Hire Date:</td>
			<td>
				<input type="date" name="hireDate" 
					value="<fmt:formatDate value="${emp.hire_date}" pattern="yyyy-MM-dd" />" 
				/>
			</td>
		</tr>
		<tr>
			<td>Salary:</td>
			<td><input type="number" name="salary" value="${emp.salary }" required/></td>
		</tr>
		<tr>
			<td>Job:</td>
			<td>
				<select name="jobId" required>
					<option value="">Select Job</option>
					<c:forEach var="job" items="${jobs}">
						<c:choose>
							<c:when test="${job.jobId == emp.job_id}">
								<option value="${job.jobId}" selected>${job.jobTitle}</option>
							</c:when>
							<c:otherwise>
								<option value="${job.jobId}">${job.jobTitle}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>Manager:</td>
			<td>
				<select name="managerId">
					<option value="">Select Manager</option>
					<c:forEach var="manager" items="${managers}">
						<c:choose>
							<c:when test="${manager.employee_id == emp.manager_id}">
								<option value="${manager.employee_id}" selected>${manager.first_name} ${manager.last_name}</option>
							</c:when>
							<c:otherwise>
								<option value="${manager.employee_id}">${manager.first_name} ${manager.last_name}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>Department:</td>
			<td>
				<select name="departmentId">
					<option value="">Select Department</option>
					<c:forEach var="department" items="${departments}">
						<c:choose>
							<c:when test="${department.departmentId == emp.department_id}">
								<option value="${department.departmentId}" selected>${department.departmentName}</option>
							</c:when>
							<c:otherwise>
								<option value="${department.departmentId}">${department.departmentName}</option>
							</c:otherwise>
						</c:choose>
						
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td align="right"><input type="submit" value="SUBMIT"/></td>
		</tr>
	</table>
		<input type="hidden" name="employee_id" value="${emp.employee_id }"/>
	</form>
	<a href="EmployeeServlet?action=list">Back</a>
</body>
</html>