<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.csc444.model.Student" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student sample</title>
<style>
	th, td {
	  padding: 5px;
	}
	</style>
</head>
<h1>Student Sample (Upload image and login)</h1>
<%
Student stud = null;
if (session.getAttribute("stud") != null) {
	stud = (Student)session.getAttribute("stud");
%>
	<h2>Welcome ${stud.username }</h2>
<%
}
else {
%>
	<h2>You are not logged in</h2>
<%
}
%>
<body>
	<table border="1" padding=10>
		<tr>
			<td>
				<a href="student_create.jsp">Register</a>
			</td>
			<td>
				Add new Student record.
			</td>
		</tr>
		<tr>
			<td>
				<a href="protected.jsp">Protected Page</a>
			</td>
			<td>
				Only logged in user can view. If not logged in, they will be redirected to login page.
			</td>
		</tr>
		<c:if test="${stud != null}">
		<tr>
			<td>
				<a href="../StudentServlet?action=view&id=${stud.studentId}">My Profile</a>
			</td>
			<td>
				This link will only appear after user logged in
			</td>
		</tr>
		</c:if>
		<c:if test="${stud != null}">
		<tr>
			<td>
				<a href="../StudentServlet?action=editprofile">Edit Profile</a>
			</td>
			<td>
				This link will only appear after user logged in
			</td>
		</tr>
		</c:if>
		<%-- display login only when session is null --%>
		<c:if test="${stud == null}">
		<tr>
			<td>
				<a href="student_login.jsp">Login</a>
			</td>
			<td>
				Login to create session
			</td>
		</tr>
		</c:if>
		<c:if test="${stud != null}">
		<tr>
			<td>
				<a href="../StudentServlet?action=logout">Logout</a>
			</td>
			<td>
				This link will only appear after user logged in
			</td>
		</tr>
		</c:if>
	</table>
</body>
</html>