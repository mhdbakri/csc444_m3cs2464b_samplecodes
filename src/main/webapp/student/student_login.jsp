<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<%-- Display insert Student form --%>
<body>
	<h1>Login</h1>
	<c:if test="${param.message != null }">
		<p style="color:red;">${param.message}</p>
	</c:if>
	<%--send form as multipart form to upload file --%>
	<form action="../StudentServlet?action=login" method="POST">
	<table>
		<tr>
			<td>Username:</td>
			<td><input type="text" name="username" required/></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type="password" name="password" required/></td>
		</tr>
		<tr>
			<td></td>
			<td align="right"><input type="submit" value="LOGIN"/></td>
		</tr>
	</table>
	</form>
	<a href="student_home.jsp">Back</a>
</body>
</html>