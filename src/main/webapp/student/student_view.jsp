<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My Profile</title>
<style type="text/css">
img.profilepic {
    max-width:64px;
    max-height:64px;
    width:auto;
    height:auto;
}
</style>
</head>
<%-- Display insert Student form --%>
<body>
	<h1>My Profile</h1>
	<table>
		<tr>
			<td>Username:</td>
			<td><b>${stud.username}</b></td>
		</tr>
		<tr>
			<td>Gender:</td>
			<td><b>${stud.gender}</b></td>
		</tr>
		<tr>
			<td>Profile Picture:</td>
			<td><b><img class="profilepic" src="upload/${stud.profilePic}"/></b></td>
		</tr>
		<tr>
			<td>Skills:</td>
			<td><b>
				<c:if test="${stud.cpp == true}">
					C++&nbsp;
				</c:if>
				<c:if test="${stud.java == true}">
					Java&nbsp;
				</c:if>
				<c:if test="${stud.python == true}">
					Python&nbsp;
				</c:if>
			</b></td>
		</tr>
	</table>
	<a href="student/student_home.jsp">Back</a>
</body>
</html>