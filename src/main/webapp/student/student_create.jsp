<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register new Student</title>
</head>
<%-- Display insert Student form --%>
<body>
	<h1>Register new Student</h1>
	<%--send form as multipart form to upload file --%>
	<form action="../StudentServlet?action=store" method="POST" enctype="multipart/form-data">
	<table>
		<tr>
			<td>Username:</td>
			<td><input type="text" name="username" required/></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type="password" name="password" required/></td>
		</tr>
		<tr>
			<td>Gender:</td>
			<td>
				<input type="radio" name="gender" value="male" required/> Male
				<input type="radio" name="gender" value="female" required/> Female
			</td>
		</tr>
		<tr>
			<td>Profile Picture:</td>
			<td>
				<input type="file" name="profilePic" required/>
			</td>
		</tr>
		<tr>
			<td>Skills:</td>
			<td>
				<input type="checkbox" name="cpp"/> C++
				<input type="checkbox" name="java"/> Java
				<input type="checkbox" name="python"/> Python
			</td>
		</tr>
		<tr>
			<td></td>
			<td align="right"><input type="submit" value="SUBMIT"/></td>
		</tr>
	</table>
	</form>
	<a href="student_home.jsp">Back</a>
</body>
</html>