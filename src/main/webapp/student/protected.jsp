<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
if (session.getAttribute("stud") == null) {
	response.sendRedirect("student_login.jsp");
}
%>
<h1>You have permission to view this page</h1>
<a href="student_home.jsp">Back</a>
</body>
</html>