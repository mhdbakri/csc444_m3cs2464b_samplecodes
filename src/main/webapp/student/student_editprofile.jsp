<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register new Student</title>
<style type="text/css">
img.profilepic {
    max-width:64px;
    max-height:64px;
    width:auto;
    height:auto;
}
</style>
</head>
<%-- Display insert Student form --%>
<body>
	<h1>Register new Student</h1>
	<%--send form as multipart form to upload file --%>
	<form action="StudentServlet?action=update" method="POST" enctype="multipart/form-data">
	<table>
		<tr>
			<td>Username:</td>
			<td><input type="text" name="username" value="${stud.username }" required/></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type="password" name="password"  required/></td>
		</tr>
		<tr>
			<td>Gender:</td>
			<td>
				<c:choose>
					<c:when test="${stud.gender == 'male' }">
						<input type="radio" name="gender" value="male" checked required/> Male
					</c:when>
					<c:otherwise>
						<input type="radio" name="gender" value="male" required/> Male
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${stud.gender == 'female' }">
						<input type="radio" name="gender" value="female" checked required/> Female
					</c:when>
					<c:otherwise>
						<input type="radio" name="gender" value="female" required/> Female
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<td>Profile Picture:</td>
			<td>
				<img class="profilepic" src="upload/${stud.profilePic}"/><br/>
				<input type="file" name="profilePic"/>
			</td>
		</tr>
		<tr>
			<td>Skills:</td>
			<td>
				<c:choose>
					<c:when test="${stud.cpp == true }">
						<input type="checkbox" name="cpp" checked/> C++
					</c:when>
					<c:otherwise>
						<input type="checkbox" name="cpp"/> C++
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${stud.java == true }">
						<input type="checkbox" name="java" checked/> Java
					</c:when>
					<c:otherwise>
						<input type="checkbox" name="java"/> Java
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${stud.python == true }">
						<input type="checkbox" name="python" checked/> Python
					</c:when>
					<c:otherwise>
						<input type="checkbox" name="python"/> Python
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<td></td>
			<td align="right"><input type="submit" value="UPDATE"/></td>
		</tr>
	</table>
	<input type="hidden" name="studentId" value="${stud.studentId }"/>
	</form>
	<a href="student_home.jsp">Back</a>
</body>
</html>