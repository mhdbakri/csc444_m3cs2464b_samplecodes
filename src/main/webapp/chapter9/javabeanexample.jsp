<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Java Bean Example</title>
</head>
<body>

	<jsp:useBean id="student" scope="request" class="com.csc444.servlet.chapter9.StudentVO" />
	
	<p>
		Name: <jsp:getProperty name="student" property="name"/>
	</p>
	<p>
		Student ID: <jsp:getProperty name="student" property="id"/>
	</p>
	<p>
		Paid: <jsp:getProperty name="student" property="paid"/>
	</p>
	
	<jsp:setProperty name="student" property="paid" value="false"/>
	<p>
		Paid After: <jsp:getProperty name="student" property="paid"/>
	</p>
	
	<table border=1>
		<thead>
			<tr>
				<td>Name</td>
				<td>Id</td>
				<td>Paid?</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="stud" items="${students }">
				<tr>
					<td><c:out value="${stud.name }"/></td>
					<td><c:out value="${stud.id }"/></td>
					<td><c:out value="${stud.paid }"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>