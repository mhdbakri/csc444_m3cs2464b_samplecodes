<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		// get parameters
		String num1Str = request.getParameter("num1");
		String operation = request.getParameter("operation");
		String num2Str = request.getParameter("num2");
		
		// check received parameters
		out.println("<h3>num1=" +num1Str+"</h3>");
		out.println("<h3>operation=" +operation+"</h3>");
		out.println("<h3>num2=" +num2Str+"</h3>");
		
		int num1 = Integer.parseInt(num1Str);
		int num2 = Integer.parseInt(num2Str);
		
		if (operation.equals("+"))
			out.println("<h2>" + num1 + " + " + num2 + " = " + (num1+num2) + "</h2>");
		else if (operation.equals("-"))
			out.println("<h2>" + num1 + " - " + num2 + " = " + (num1-num2) + "</h2>");
	%>
</body>
</html>