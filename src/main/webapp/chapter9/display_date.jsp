<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Display today's date</title>
</head>
<body>
	<!-- To print today's date -->
	<h1>Today's Date</h1>
	<h2><%=new java.util.Date() %></h2>
	
	<%-- to calculate the sum of a and b --%>
	<%
		int a = 10;
		int b = 12;
		int sum = a + b;
	%>
	<h2>Sum=<%=sum %></h2>
	
	<% for (int i=1; i<=10; i++) { %>
		<h2><%=i%>*<%=i%> = <%=square(i) %></h2>
	<% } %>
	
	<%! public int square(int n) {
			return n*n;
		}
	%>
</body>
</html>