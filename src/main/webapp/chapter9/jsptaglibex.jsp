<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSP taglib example</title>
</head>
<body>
	<%--Print table using JAVA expression --%>
	<table border="1">
	<%
		String cars[] = (String[])request.getAttribute("cars");
		for (int i=0; i<cars.length; i++) {
			out.println("<tr>");
			out.println("<td>"+cars[i]+"</td>");
			out.println("</tr>");
		}
	%>
	</table>

	<br />
	
	<%--Looping using JSTL tags --%>
	<table border="1">
	<c:forEach var="car" items="${cars}">
		<tr>
			<td>${car}</td>
		</tr>
	</c:forEach>
	</table>
	
	<br /><br />
	<%--selection using expression --%>
	<%
		int num = (int)request.getAttribute("num");
		if (num % 2 == 0)
			out.println(num + " is even.");
		else
			out.println(num + " is odd.");
	%>
	
	<br />
	
	<%--selection using JSTL tag --%>
	<c:if test="${num % 2 == 0 }">
		<c:out value="${num }"/> is even.
	</c:if>
	<c:if test="${num % 2 != 0 }">
		<c:out value="${num }"/> is odd.
	</c:if>
	
	<br />
	<%--selection using JSTL tag --%>
	<c:choose>
		<c:when test="${num % 2 == 0 }">
			<c:out value="${num }"/> is even.
		</c:when>
		<c:when test="${num % 2 != 0 }">
			<c:out value="${num }"/> is odd.
		</c:when>
	</c:choose>
</body>
</html>