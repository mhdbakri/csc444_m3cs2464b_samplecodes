package com.csc444.servlet.chapter8;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MyCalculator
 */
@WebServlet("/MyCalculator")
public class MyCalculator extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public MyCalculator() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		pw.println("<html>");
		pw.println("<table border='1'>");

		pw.println("<tr><td><h1>Hello world</h1></td></tr>");
		pw.println("<tr><td><h2>Hello again</h2></td></tr>");
		
		String numStr = request.getParameter("number");
		pw.println("<tr><td><h2>number: " + numStr + "</h2></td></tr>");
		
		int num = Integer.parseInt(numStr);
		pw.println("<tr><td><h2>"+num+ " * 12 = " + (num*12) + "</h2></td></tr>");
		
		pw.println("</table>");
		pw.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		pw.println("<html>");
		pw.println("<h1>Hello world</h1>");
		pw.println("<h2>Hello again</h2>");
		
		String numStr = request.getParameter("number");
		pw.println("<h2>number: " + numStr + "</h2>");
		
		int num = Integer.parseInt(numStr);
		pw.println("<h2>"+num+ " * 12 = " + (num*12) + "</h2>");
		
		pw.println("</html>");
	}

}
