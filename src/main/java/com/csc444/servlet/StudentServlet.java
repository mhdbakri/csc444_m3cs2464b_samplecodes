package com.csc444.servlet;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import com.csc444.dao.EmployeeDAO;
import com.csc444.dao.StudentDAO;
import com.csc444.model.Employee;
import com.csc444.model.Student;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
@MultipartConfig(
		fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
		maxFileSize = 1024 * 1024 * 10,      // 10 MB
		maxRequestSize = 1024 * 1024 * 100   // 100 MB
)
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final String UPLOAD_DIR = "C:\\upload\\";

    /**
     * Default constructor. 
     */
    public StudentServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if (action.equals("view")) {
			viewStudent(request, response);
		}
		else if (action.equals("logout")) {
			logoutStudent(request, response);
		}
		else if (action.equals("editprofile")) {
			editProfile(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if (action.equals("store")) {
			insertStudent(request, response);
		}
		else if (action.equals("login")) {
			loginStudent(request, response);
		}
		else if (action.equals("update")) {
			updateProfile(request, response);
		}
	}
	
	protected void insertStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get form values
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String gender = request.getParameter("gender");
		
		// store uploaded file
		Part filePart = request.getPart("profilePic");
		String uploadedFileName = processAttachment(filePart);
		
		// get checkboxes value
		boolean cpp = false, java = false, python = false;
		if (request.getParameter("cpp") != null)
			cpp = true;
		if (request.getParameter("java") != null)
			java = true;
		if (request.getParameter("python") != null)
			python = true;
		
		// create Student object
		Student s = new Student(-1, username, password, gender, uploadedFileName, cpp, java, python);
		
		try {
			// store to database
			int studId = StudentDAO.insertStudent(s);
			
			// redirect user to employee detail page
			response.sendRedirect("StudentServlet?action=view&id=" + studId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	/**
     * Handle uploaded files
     * @param filePart upload file
     * @return file is uploaded successfully
     */
    private String processAttachment (Part filePart) throws IOException
    {
        InputStream inputStream = filePart.getInputStream ();
        //append system milliseconds to the new file name
        String fileName = System.currentTimeMillis() + "_" + filePart.getSubmittedFileName();
        File file = new File (UPLOAD_DIR + fileName); 
        
        // write to upload folder
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream (new FileOutputStream (file));
        int read;
        final byte [] bytes = new byte [1024];
        while ((read = inputStream.read (bytes)) != -1)
        {
            bufferedOutputStream.write (bytes, 0, read);
        }
        bufferedOutputStream.flush ();//Must remember to flush, otherwise it may cause file corruption
        bufferedOutputStream.close();
        
        return fileName;
    }
    
    /**
     * Display info of the student based on the id
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void viewStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// get Student record based on the id
			Student stud = StudentDAO.getStudent(id);
			
			// set attribute
			request.setAttribute("stud", stud);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// forward to JSP
		RequestDispatcher requestDispatcher = request
				.getRequestDispatcher("student/student_view.jsp");
		requestDispatcher.forward(request, response);
	}
    
    /**
     * Login using username and password. If successful, put student object into session
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void loginStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	String username = request.getParameter("username");
    	String password = request.getParameter("password");
    	
		try {
			// login based on username and password
			Student stud = StudentDAO.login(username, password);
			
			if (stud != null) {
			
				// set attribute
				HttpSession session = request.getSession();
				session.setAttribute("stud", stud);
				
				// you can also set role attribute to the session to make it easier
				// for you to check type of user that is logged in
				// is it admin or normal user
				
				// forward to JSP
				response.sendRedirect("student/student_home.jsp");
			}
			else {
				response.sendRedirect("student/student_login.jsp?message=Invalid username/password");

			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
	}
    
    /**
     * Logout student. Invalidate the session
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void logoutStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	request.getSession().invalidate();	// destroy session
    	
    	// forward to home
		response.sendRedirect("student/student_home.jsp");

		
	}
    
    /**
     * Request edit profile form. Get the current profile info and forward to edit jsp.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void editProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	// get id from session
    	HttpSession session = request.getSession();
    	// only logged in user can update profile
    	if (session.getAttribute("stud") != null) {
	    	Student stud = (Student)session.getAttribute("stud");
	
			// set attribute
	    	try {
				stud = StudentDAO.getStudent(stud.getStudentId());
				request.setAttribute("stud", stud);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // get latest record
			
	
			// forward to JSP
			RequestDispatcher requestDispatcher = request
					.getRequestDispatcher("student/student_editprofile.jsp");
			requestDispatcher.forward(request, response);
    	}
	}
    
    /**
     * Update user profile to the database. If new profile pic image is not selected, 
     * don't update the field
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void updateProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get form values
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String gender = request.getParameter("gender");
		
		// hidden field studentId or you can just retrieve the id from session
		int studentId = Integer.parseInt(request.getParameter("studentId"));
		
		// store uploaded file, if user choose new profile pic
		String uploadedFileName = null;
		Part filePart = request.getPart("profilePic");
		if (filePart.getSize() > 0 )
			uploadedFileName = processAttachment(filePart);
		
		// get checkboxes value
		boolean cpp = false, java = false, python = false;
		if (request.getParameter("cpp") != null)
			cpp = true;
		if (request.getParameter("java") != null)
			java = true;
		if (request.getParameter("python") != null)
			python = true;
		
		// create Student object
		Student s = new Student(studentId, username, password, gender, uploadedFileName, cpp, java, python);
		
		try {
			// store to database
			boolean success = StudentDAO.updateStudent(s);
			
			// redirect user to employee detail page
			response.sendRedirect("StudentServlet?action=view&id=" + studentId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}


}
