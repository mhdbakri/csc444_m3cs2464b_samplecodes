package com.csc444.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloWorld
 */
@WebServlet("/HelloWorld")
public class HelloWorld extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public HelloWorld() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer = response.getWriter();
		writer.println("<html><body>");
		writer.println("<h1>Hello World!!!!</h1>");
		writer.println("<h2>bakri (c)</h2>");
		writer.println("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer = response.getWriter();
		writer.println("<html><body>");
		writer.println("<h1>Inside doPost method</h1>");
		
		String name = request.getParameter("name");
		String bdate = request.getParameter("birthdate");
		String program = request.getParameter("program");
		String gender = request.getParameter("gender");
		String cpp = request.getParameter("cpp");
		String java = request.getParameter("java");
		
		writer.println("<b>Name:</b> " + name + "<br/>");
		writer.println("<b>Birth Date:</b> " + bdate + "<br/>");
		writer.println("<b>Program:</b> " + program + "<br/>");
		
		if (gender ==  null)
			writer.println("<b>Please select your gender<br/>");
		else
			writer.println("<b>Gender:</b> " + gender + "<br/>");
		
		if (cpp != null)
			writer.println("<b>You know C++<br/>");
		
		if (java != null)
			writer.println("<b>You know JAVA<br/>");
		
		writer.println("</body></html>");
	}

}
