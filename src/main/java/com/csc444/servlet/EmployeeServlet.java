package com.csc444.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.csc444.dao.DepartmentDAO;
import com.csc444.dao.EmployeeDAO;
import com.csc444.dao.JobDAO;
import com.csc444.model.Department;
import com.csc444.model.Employee;
import com.csc444.model.Job;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EmployeeServlet
 */
@WebServlet("/EmployeeServlet")
public class EmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public EmployeeServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("list")) {
			listAllEmployees(request, response);
		}
		else if (action.equals("view")) {
			viewEmployee(request, response);
		}
		else if (action.equals("create")) {
			createEmployee(request, response);
		}
		else if (action.equals("delete")) {
			deleteEmployee(request, response);
		}
		else if (action.equals("edit")) {
			editEmployee(request, response);
		}
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("store")) {
			storeEmployee(request, response);
		}
		else if (action.equals("update")) {
			updateEmployee(request, response);
		}
	}

	/**
	 * List all employees record
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void listAllEmployees(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			// get all employees record
			List<Employee> employees = EmployeeDAO.listAllEmployees();
			
			// set attribute
			request.setAttribute("employees", employees);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// forward to JSP
		RequestDispatcher requestDispatcher = request
				.getRequestDispatcher("employee/employee_index.jsp");
		requestDispatcher.forward(request, response);
	}
	
	protected void viewEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// get Employee record based on the id
			Employee empl = EmployeeDAO.getEmployee(id);
			
			// set attribute
			request.setAttribute("empl", empl);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// forward to JSP
		RequestDispatcher requestDispatcher = request
				.getRequestDispatcher("employee/employee_view.jsp");
		requestDispatcher.forward(request, response);
	}
	
	protected void createEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			// get list of FK
			List<Employee> managers = EmployeeDAO.listAllEmployees();
			List<Job> jobs = JobDAO.getAllJobs();
			List<Department> departments = DepartmentDAO.getAllDepartments();
			
			// set attributes
			request.setAttribute("managers", managers);
			request.setAttribute("jobs", jobs);
			request.setAttribute("departments", departments);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request
					.getRequestDispatcher("employee/employee_create.jsp");
			requestDispatcher.forward(request, response);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	protected void storeEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// get the form fields
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String email = request.getParameter("email");
			String phoneNumber = request.getParameter("phoneNumber");
			String hireDateStr = request.getParameter("hireDate");
			String salaryStr = request.getParameter("salary");
			String jobIdStr = request.getParameter("jobId");
			String managerIdStr = request.getParameter("managerId");
			String departmentIdStr = request.getParameter("departmentId");
			
			// set to object
			Employee emp = new Employee();
			emp.setFirst_name(firstName);
			emp.setLast_name(lastName);
			emp.setEmail(email);
			emp.setPhone_number(phoneNumber);
			
			Date hireDate=new SimpleDateFormat("yyyy-MM-dd").parse(hireDateStr);
			emp.setHire_date(hireDate);
			
			double salary = Double.parseDouble(salaryStr);
			emp.setSalary(salary);
			
			int jobId = -1;
			if (!jobIdStr.equals(""))
				jobId = Integer.parseInt(jobIdStr);
			emp.setJob_id(jobId);
			
			int managerId = -1;
			if (!managerIdStr.equals(""))
				managerId = Integer.parseInt(managerIdStr);
			emp.setManager_id(managerId);
			
			int departmentId = -1;
			if (!departmentIdStr.equals(""))
				departmentId = Integer.parseInt(departmentIdStr);
			emp.setDepartment_id(departmentId);
			
			// store to DB
			int empId = EmployeeDAO.storeEmployee(emp);
			
			// redirect user to employee detail page
			response.sendRedirect("EmployeeServlet?action=view&id=" + empId);
			
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		

	}
	
	protected void updateEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// get the form fields
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String email = request.getParameter("email");
			String phoneNumber = request.getParameter("phoneNumber");
			String hireDateStr = request.getParameter("hireDate");
			String salaryStr = request.getParameter("salary");
			String jobIdStr = request.getParameter("jobId");
			String managerIdStr = request.getParameter("managerId");
			String departmentIdStr = request.getParameter("departmentId");
			int employee_id = Integer.parseInt(request.getParameter("employee_id"));
			
			// set to object
			Employee emp = new Employee();
			emp.setEmployee_id(employee_id);
			emp.setFirst_name(firstName);
			emp.setLast_name(lastName);
			emp.setEmail(email);
			emp.setPhone_number(phoneNumber);
			
			Date hireDate=new SimpleDateFormat("yyyy-MM-dd").parse(hireDateStr);
			emp.setHire_date(hireDate);
			
			double salary = Double.parseDouble(salaryStr);
			emp.setSalary(salary);
			
			int jobId = -1;
			if (!jobIdStr.equals(""))
				jobId = Integer.parseInt(jobIdStr);
			emp.setJob_id(jobId);
			
			int managerId = -1;
			if (!managerIdStr.equals(""))
				managerId = Integer.parseInt(managerIdStr);
			emp.setManager_id(managerId);
			
			int departmentId = -1;
			if (!departmentIdStr.equals(""))
				departmentId = Integer.parseInt(departmentIdStr);
			emp.setDepartment_id(departmentId);
			
			// store to DB
			boolean updateStatus = EmployeeDAO.updateEmployee(emp);
			
			// redirect user to employee detail page
			response.sendRedirect("EmployeeServlet?action=view&id=" + employee_id);
			
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		

	}
	
	protected void deleteEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// delete Employee record based on the id
			EmployeeDAO.deleteEmployee(id);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// redirect user to employee list page
		response.sendRedirect("EmployeeServlet?action=list");
	}
	
	protected void editEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get employee id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// get employee record
			Employee emp = EmployeeDAO.getEmployee(id);
			
			// set to request attribute
			request.setAttribute("emp", emp);
			
			// get list of FK
			List<Employee> managers = EmployeeDAO.listAllEmployees();
			List<Job> jobs = JobDAO.getAllJobs();
			List<Department> departments = DepartmentDAO.getAllDepartments();
						
			// set attributes
			request.setAttribute("managers", managers);
			request.setAttribute("jobs", jobs);
			request.setAttribute("departments", departments);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request
					.getRequestDispatcher("employee/employee_update.jsp");
			requestDispatcher.forward(request, response);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	

}
