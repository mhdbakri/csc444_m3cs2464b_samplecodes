package com.csc444.servlet.chapter9;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class JspTaglibEx
 */
@WebServlet("/JspTaglibEx")
public class JspTaglibEx extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public JspTaglibEx() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// set data in request
		String cars[] = {"Ferrari", "Porshe", "Toyota"};
		request.setAttribute("cars", cars);
		
		int num = 9;
		request.setAttribute("num", num);
		
		// forward to JSP
		RequestDispatcher requestDispatcher = request
                .getRequestDispatcher("chapter9/jsptaglibex.jsp");
        requestDispatcher.forward(request, response);
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
