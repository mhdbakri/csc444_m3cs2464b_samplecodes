package com.csc444.servlet.chapter9;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class JavaBeanExample
 */
@WebServlet("/JavaBeanExample")
public class JavaBeanExample extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public JavaBeanExample() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// create dummy data
		StudentVO stud = new StudentVO();
		stud.setName("Mahmud");
		stud.setId(1311);
		stud.setPaid(true);
		request.setAttribute("student", stud);
		
		StudentVO studs[] = new StudentVO[3];
		
		studs[0] = new StudentVO();
		studs[0].setName("Mahmud");
		studs[0].setId(1311);
		studs[0].setPaid(true);

		studs[1] = new StudentVO();
		studs[1].setName("Helen");
		studs[1].setId(3545);
		studs[1].setPaid(true);

		studs[2] = new StudentVO();
		studs[2].setName("Jamil");
		studs[2].setId(5675);
		studs[2].setPaid(false);
		
		request.setAttribute("students", studs);
		
		// forward to JSP
		RequestDispatcher requestDispatcher = request
				.getRequestDispatcher("chapter9/javabeanexample.jsp");
		requestDispatcher.forward(request, response);
	}



}
