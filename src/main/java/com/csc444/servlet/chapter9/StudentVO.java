package com.csc444.servlet.chapter9;

public class StudentVO {
	
	private String name;
	private int id;
	private boolean paid;
	
	public StudentVO() {
		name = "";
		id = 0;
		paid = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	@Override
	public String toString() {
		return "StudentVO [name=" + name + ", id=" + id + ", paid=" + paid + "]";
	}
	
	

}
