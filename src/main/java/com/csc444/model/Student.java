package com.csc444.model;

public class Student {
	
	private int studentId;
	private String username;
	private String password;
	private String gender;
	private String profilePic; // file name
	private boolean cpp;
	private boolean java;
	private boolean python;
	
	public Student() {
	}

	public Student(int studentId, String username, String password, String gender, String profilePic, boolean cpp,
			boolean java, boolean python) {
		this.studentId = studentId;
		this.username = username;
		this.password = password;
		this.gender = gender;
		this.profilePic = profilePic;
		this.cpp = cpp;
		this.java = java;
		this.python = python;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public boolean isCpp() {
		return cpp;
	}

	public void setCpp(boolean cpp) {
		this.cpp = cpp;
	}

	public boolean isJava() {
		return java;
	}

	public void setJava(boolean java) {
		this.java = java;
	}

	public boolean isPython() {
		return python;
	}

	public void setPython(boolean python) {
		this.python = python;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", username=" + username + ", password=" + password + ", gender="
				+ gender + ", profilePic=" + profilePic + ", cpp=" + cpp + ", java=" + java + ", python=" + python
				+ "]";
	}

}
