package com.csc444.model;

/**
 * Department model
 * @author User
 *
 */
public class Department {
	
	private int departmentId;
	private String departmentName;
	
	// FK
	private int locationId;
	// private Location location;
	
	public Department() {
	}

	public Department(int departmentId, String departmentName, int locationId) {
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.locationId = locationId;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	@Override
	public String toString() {
		return "Department [departmentId=" + departmentId + ", departmentName=" + departmentName + ", locationId="
				+ locationId + "]";
	}
	
	

}
