package com.csc444.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.csc444.model.Job;
import com.csc444.util.DBUtil;

/**
 * To manage Jobs table
 * 
 * @author User
 *
 */
public class JobDAO {

	/**
	 * Retrieve list of Job records from jobs table.
	 * 
	 * @return list of job records
	 * @throws SQLException
	 */
	public static List<Job> getAllJobs() throws SQLException {

		// initialize an empty ArrayList
		List<Job> listJobs = new ArrayList<>();

		// the SQL query
		String sql = "SELECT * FROM jobs";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			statement = connection.createStatement();
			// execute query
			resultSet = statement.executeQuery(sql);

			// loop for each record selected
			while (resultSet.next()) {
				// get values for this record
				int jobId = resultSet.getInt("job_id");
				String jobTitle = resultSet.getString("job_title");
				double minSalary = resultSet.getDouble("min_salary");
				double maxSalary = resultSet.getDouble("max_salary");

				// create Job bean object based on this record
				Job job = new Job(jobId, jobTitle, minSalary, maxSalary);

				// add this Employee bean object to the ArrayList
				listJobs.add(job);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (statement != null)
				statement.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the ArrayList with list of Jobs object
		return listJobs;

	}

}
