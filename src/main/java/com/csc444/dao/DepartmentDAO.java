package com.csc444.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.csc444.model.Department;
import com.csc444.util.DBUtil;

public class DepartmentDAO {

	/**
	 * Retrieve list of department records
	 * @return array list of Department records
	 * @throws SQLException
	 */
	public static List<Department> getAllDepartments() throws SQLException {

		// initialize an empty ArrayList
		List<Department> listDepartments = new ArrayList<>();

		// the SQL query
		String sql = "SELECT * FROM departments";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			statement = connection.createStatement();
			// execute query
			resultSet = statement.executeQuery(sql);

			// loop for each record selected
			while (resultSet.next()) {
				// get values for this record
				int departmentId = resultSet.getInt("department_id");
				String departmentName = resultSet.getString("department_name");
				int locationId = resultSet.getInt("location_id");

				// create Job bean object based on this record
				Department department = new Department(departmentId, departmentName, locationId);

				// add this Department bean object to the ArrayList
				listDepartments.add(department);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (statement != null)
				statement.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the ArrayList with list of Departments object
		return listDepartments;

	}
}
