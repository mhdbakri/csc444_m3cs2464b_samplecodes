package com.csc444.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.csc444.model.Employee;
import com.csc444.model.Student;
import com.csc444.util.DBUtil;
import com.csc444.utils.MyUtils;

public class StudentDAO {

	/**
	 * Check username and password
	 * @param username
	 * @param password
	 * @return student object if match a record. null otherwise
	 * @throws SQLException
	 */
	public static Student login(String username, String password) throws SQLException {
		// Student bean to be returned, default is null
		Student stud = null;

		// the SQL query
		String sql = "SELECT * FROM students where username=? AND password=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			
			// set parameter in the query
			ps.setString(1, username);
			
			// hash the password first
			String hashedPass = MyUtils.toMD5(password);
			ps.setString(2, hashedPass);
			
			// execute the query
			resultSet = ps.executeQuery();

			// check if username and password matched any record
			if (resultSet.next()) {
				// get values for this record.
				// skip password because we will not display it anywhere
				// in the web system
				int studId = resultSet.getInt("student_id");
				String gender = resultSet.getString("gender");
				String profile_pic = resultSet.getString("profile_pic");
				boolean cpp = resultSet.getBoolean("cpp");
				boolean java = resultSet.getBoolean("java");
				boolean python = resultSet.getBoolean("python");

				// create Student bean object based on this record
				stud = new Student(studId, username, null, gender, profile_pic, cpp, java, python);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the Student bean
		return stud;
	}
	
	/**
	 * Get a single Student record based on the id
	 * 
	 * @param studId
	 * @return student object
	 * @throws SQLException
	 */
	public static Student getStudent(int studId) throws SQLException {
		// Student bean to be returned, default is null
		Student stud = null;

		// the SQL query
		String sql = "SELECT * FROM students where student_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, studId);
			// execute the query
			resultSet = ps.executeQuery();

			// check if any record returned
			if (resultSet.next()) {
				// get values for this record.
				// skip password because we will not display it anywhere
				// in the web system
				String username = resultSet.getString("username");
				String gender = resultSet.getString("gender");
				String profile_pic = resultSet.getString("profile_pic");
				boolean cpp = resultSet.getBoolean("cpp");
				boolean java = resultSet.getBoolean("java");
				boolean python = resultSet.getBoolean("python");

				// create Student bean object based on this record
				stud = new Student(studId, username, null, gender, profile_pic, cpp, java, python);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the Student bean
		return stud;
	}

	/**
	 * Get all student record in database
	 * 
	 * @return
	 * @throws SQLException
	 */
	private static List<Student> getAllStudents() throws SQLException {
		// initialize an empty ArrayList
		List<Student> studentList = new ArrayList<>();

		// the SQL query
		String sql = "SELECT * FROM students";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			statement = connection.createStatement();
			// execute query
			resultSet = statement.executeQuery(sql);

			// loop for each record selected
			while (resultSet.next()) {
				// get values for this record.
				// skip password because we will not display it anywhere
				// in the web system
				int studId = resultSet.getInt("student_id");
				String username = resultSet.getString("username");
				String gender = resultSet.getString("gender");
				String profile_pic = resultSet.getString("profile_pic");
				boolean cpp = resultSet.getBoolean("cpp");
				boolean java = resultSet.getBoolean("cpp");
				boolean python = resultSet.getBoolean("python");

				// create Student bean object based on this record
				Student student = new Student(studId, username, null, gender, profile_pic, cpp, java, python);

				// add this Student bean object to the ArrayList
				studentList.add(student);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (statement != null)
				statement.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the ArrayList with list of Employee object
		return studentList;
	}

	/**
	 * Update student record. Return true if success, false if not
	 */
	public static boolean updateStudent(Student student) throws SQLException {
		boolean success = false;

		// the SQL query
		String sql = null;
		if (student.getProfilePic() != null) {
			// new profile pic
			sql = "update STUDENTS set username=?, password=?, gender=?,"
				+ "profile_pic=?, cpp=?, java=?, python=?";
		}
		else {
			// old profile pic, do not change
			sql = "update STUDENTS set username=?, password=?, gender=?,"
					+ "cpp=?, java=?, python=?";
		}
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			// set parameter in the query
			// since all fields are required, no need to check empty value
			// and set null
			ps.setString(1, student.getUsername());

			// hash password for security purpose.
			// good practice, password is not reversible
			String hashedPass = MyUtils.toMD5(student.getPassword());
			ps.setString(2, hashedPass);

			if (student.getProfilePic() != null) {
				ps.setString(3, student.getGender());
				ps.setString(4, student.getProfilePic());
				ps.setBoolean(5, student.isCpp());
				ps.setBoolean(6, student.isJava());
				ps.setBoolean(7, student.isPython());
			}
			else {
				ps.setString(3, student.getGender());
				ps.setBoolean(4, student.isCpp());
				ps.setBoolean(5, student.isJava());
				ps.setBoolean(6, student.isPython());
			}

			ps.executeUpdate();
			success = true;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return update status
		return success;
	}

	/**
	 * Add new student record. Return the generated student id if successful add.
	 * return -1 if fail to add.
	 */
	public static int insertStudent(Student student) throws SQLException {
		// to store student id of this new student
		int studId = -1;

		// the SQL query
		String sql = "insert into STUDENTS(username, password, gender," + "profile_pic, cpp, java, python) "
				+ "values (?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			// set parameter in the query
			// since all fields are required, no need to check empty value
			// and set null
			ps.setString(1, student.getUsername());

			// hash password for security purpose.
			// good practice, password is not reversible
			String hashedPass = MyUtils.toMD5(student.getPassword());
			ps.setString(2, hashedPass);

			ps.setString(3, student.getGender());
			ps.setString(4, student.getProfilePic());
			ps.setBoolean(5, student.isCpp());
			ps.setBoolean(6, student.isJava());
			ps.setBoolean(7, student.isPython());
			
			// execute insert
			ps.executeUpdate();

			// get generated student id
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			studId = rs.getInt(1);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return student id of this new student to caller. -1 if error
		return studId;
	}

	public static boolean deleteStudent(int studId) throws SQLException {

		// delete status
		boolean success = false;

		// the SQL query
		String sql = "DELETE FROM students where student_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, studId);
			// execute the query
			ps.executeUpdate();

			success = true; // successful delete the record

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the status
		return success;

	}
}
