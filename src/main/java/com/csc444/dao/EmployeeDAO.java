package com.csc444.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.csc444.model.Employee;
import com.csc444.util.DBUtil;

public class EmployeeDAO {

	/**
	 * Get all employees record from database
	 * 
	 * @return List of Employee objects
	 * @throws SQLException
	 */
	public static List<Employee> listAllEmployees() throws SQLException {

		// initialize an empty ArrayList
		List<Employee> listEmployee = new ArrayList<>();

		// the SQL query
		String sql = "SELECT * FROM employees";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			statement = connection.createStatement();
			// execute query
			resultSet = statement.executeQuery(sql);

			// loop for each record selected
			while (resultSet.next()) {
				// get values for this record
				int employee_id = resultSet.getInt("employee_id");
				String first_name = resultSet.getString("first_name");
				String last_name = resultSet.getString("last_name");
				String email = resultSet.getString("email");
				String phone_number = resultSet.getString("phone_number");
				Date hire_date = resultSet.getTimestamp("hire_date");
				double salary = resultSet.getDouble("salary");
				int job_id = resultSet.getInt("job_id");
				int manager_id = resultSet.getInt("manager_id");
				int department_id = resultSet.getInt("department_id");

				// create Employee bean object based on this record
				Employee emp = new Employee(employee_id, first_name, last_name, email, phone_number, hire_date, salary,
						job_id, manager_id, department_id);

				// set foreign key objects (good practice)
				if (manager_id != 0) {
					Employee manager = getEmployee(manager_id);
					emp.setManager(manager);
				}

				// add this Employee bean object to the ArrayList
				listEmployee.add(emp);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (statement != null)
				statement.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the ArrayList with list of Employee object
		return listEmployee;

	}

	/**
	 * Get a single Employee bean based on the employee_id
	 * 
	 * @param id employee id
	 * @return Employee bean based on the id. null if not found
	 * @throws SQLException
	 */
	public static Employee getEmployee(int id) throws SQLException {

		// Employee bean to be returned, default is null
		Employee emp = null;

		// the SQL query
		String sql = "SELECT * FROM employees where employee_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			resultSet = ps.executeQuery();

			// check if any record returned
			if (resultSet.next()) {
				// if there is a record, get the data
				int employee_id = resultSet.getInt("employee_id");
				String first_name = resultSet.getString("first_name");
				;
				String last_name = resultSet.getString("last_name");
				;
				String email = resultSet.getString("email");
				;
				String phone_number = resultSet.getString("phone_number");
				;
				Date hire_date = resultSet.getTimestamp("hire_date");
				double salary = resultSet.getDouble("salary");
				;
				int job_id = resultSet.getInt("job_id");
				int manager_id = resultSet.getInt("manager_id");
				int department_id = resultSet.getInt("department_id");

				// create an Employee bean based on the data
				emp = new Employee(employee_id, first_name, last_name, email, phone_number, hire_date, salary, job_id,
						manager_id, department_id);

				// set foreign key objects (good practice)
				if (manager_id != 0) {
					Employee manager = getEmployee(manager_id);
					emp.setManager(manager);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the Employee bean
		return emp;

	}

	/**
	 * Store new employee record
	 * 
	 * @param emp - Employee object
	 * @return generated employee id
	 * @throws SQLException
	 */
	public static int storeEmployee(Employee emp) throws SQLException {

		// to store employee id of this new employee
		int empId = -1;

		// the SQL query
		String sql = "insert into Employees(first_name, last_name" + ", email, phone_number, hire_date, job_id, salary"
				+ ", manager_id, department_id) values (?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			// set parameter in the query
			if (!emp.getFirst_name().equals(""))
				ps.setString(1, emp.getFirst_name());
			else
				ps.setNull(1, java.sql.Types.VARCHAR);

			if (!emp.getLast_name().equals(""))
				ps.setString(2, emp.getLast_name());
			else
				ps.setNull(2, java.sql.Types.VARCHAR);

			if (!emp.getEmail().equals(""))
				ps.setString(3, emp.getEmail());
			else
				ps.setNull(3, java.sql.Types.VARCHAR);

			if (!emp.getPhone_number().equals(""))
				ps.setString(4, emp.getPhone_number());
			else
				ps.setNull(4, java.sql.Types.VARCHAR);

			if (emp.getHire_date() != null) {
				// need to convert from java.util.Date to java.sql.Date
				java.sql.Date sqlDate = new java.sql.Date(emp.getHire_date().getTime());
				ps.setDate(5, sqlDate);
			} else {
				ps.setNull(5, java.sql.Types.DATE);
			}

			if (emp.getJob_id() != -1)
				ps.setInt(6, emp.getJob_id());
			else
				ps.setNull(6, java.sql.Types.INTEGER);

			ps.setDouble(7, emp.getSalary());

			if (emp.getManager_id() != -1)
				ps.setInt(8, emp.getManager_id());
			else
				ps.setNull(8, java.sql.Types.INTEGER);

			if (emp.getDepartment_id() != -1)
				ps.setInt(9, emp.getDepartment_id());
			else
				ps.setNull(9, java.sql.Types.INTEGER);

			// execute the query
			ps.executeUpdate();

			// get generated employee id
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			empId = rs.getInt(1);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return employee id of this new employee to caller. -1 if error
		return empId;

	}

	public static boolean deleteEmployee(int id) throws SQLException {

		// delete status
		boolean success = false;

		// the SQL query
		String sql = "DELETE FROM employees where employee_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			ps.executeUpdate();

			success = true; // successful delete the record

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the status
		return success;

	}

	public static boolean updateEmployee(Employee emp) throws SQLException {

		int numRec = 0; // records updated

		// the SQL query
		String sql = "update Employees set first_name=?, last_name=?"
				+ ", email=?, phone_number=?, hire_date=?, job_id=?, salary=?"
				+ ", manager_id=?, department_id=? where employee_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			// set parameter in the query
			if (!emp.getFirst_name().equals(""))
				ps.setString(1, emp.getFirst_name());
			else
				ps.setNull(1, java.sql.Types.VARCHAR);

			if (!emp.getLast_name().equals(""))
				ps.setString(2, emp.getLast_name());
			else
				ps.setNull(2, java.sql.Types.VARCHAR);

			if (!emp.getEmail().equals(""))
				ps.setString(3, emp.getEmail());
			else
				ps.setNull(3, java.sql.Types.VARCHAR);

			if (!emp.getPhone_number().equals(""))
				ps.setString(4, emp.getPhone_number());
			else
				ps.setNull(4, java.sql.Types.VARCHAR);

			if (emp.getHire_date() != null) {
				// need to convert from java.util.Date to java.sql.Date
				java.sql.Date sqlDate = new java.sql.Date(emp.getHire_date().getTime());
				ps.setDate(5, sqlDate);
			} else {
				ps.setNull(5, java.sql.Types.DATE);
			}

			if (emp.getJob_id() != -1)
				ps.setInt(6, emp.getJob_id());
			else
				ps.setNull(6, java.sql.Types.INTEGER);

			ps.setDouble(7, emp.getSalary());

			if (emp.getManager_id() != -1)
				ps.setInt(8, emp.getManager_id());
			else
				ps.setNull(8, java.sql.Types.INTEGER);

			if (emp.getDepartment_id() != -1)
				ps.setInt(9, emp.getDepartment_id());
			else
				ps.setNull(9, java.sql.Types.INTEGER);

			ps.setInt(10, emp.getEmployee_id());

			// execute the query
			numRec = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		if (numRec > 0)
			return true;
		else
			return false;

	}

}
